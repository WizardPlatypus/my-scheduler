﻿using System;

using System.Windows.Controls;
using System.Windows.Media;

namespace MyScheduler
{
    internal class NumberScroller : TextBox
    {
        private int my_number = 0;

        private string MyNumberAsString()
        {
            if (my_number == 0)
                return "00";
            if (my_number < 10)
                return "0" + my_number.ToString();
            return my_number.ToString();
        }

        private void FixMyNumber()
        {
            if (my_number < MinValue)
                my_number += MaxValue + 1;
            if (my_number > MaxValue)
                my_number -= MaxValue + 1;
        }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int Step { get; set; }
        public int Number
        {
            get => my_number;
            set
            {
                my_number = value;
                FixMyNumber();
                Text = MyNumberAsString();
            }
        }
        public override void BeginInit()
        {
            base.BeginInit();
            //Height = 80;
            //Width = 80;
            FontFamily = new System.Windows.Media.FontFamily("Digital-7 Mono");
            FontSize = 90;
            BorderThickness = new System.Windows.Thickness(3);
            BorderBrush = new SolidColorBrush(Colors.Black);
            Foreground = new SolidColorBrush(Colors.Black);
            Text = MyNumberAsString();
            HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
            IsReadOnly = true;
            MouseWheel += OnWheel;
            MinValue = 0;
            MaxValue = 100;
            Step = 1;
        }
        private void OnWheel(object sender,
            System.Windows.Input.MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
                Number += Step;
            if (e.Delta < 0)
                Number -= Step;
            UserChangedNumber(this, new UserChangedNumberEventArgs(Number));
        }

        public class UserChangedNumberEventArgs : EventArgs
        {
            public int NewNumber { get; set; }
            public UserChangedNumberEventArgs(int new_number)
            {
                NewNumber = new_number;
            }
        }
        public event EventHandler<UserChangedNumberEventArgs>
            UserChangedNumber;
    }
}
