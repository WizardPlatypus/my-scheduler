﻿//#define QUICK_PASSWORD

using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Threading;

namespace MyScheduler
{
    public class ScreenLocker : Window
    {
        public ScreenLocker()
        {
            AllowsTransparency = true;
            Topmost = true;
            ShowInTaskbar = false;
            Background = Brushes.Transparent;
            WindowStyle = WindowStyle.None;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            WindowState = WindowState.Maximized;

            var brush = new SolidColorBrush(Color.FromRgb(0x41, 0xb7, 0xd7));
            _background = new Rectangle() { Fill = brush, Opacity = 0.5 };
            _text = new TextBlock()
            {
                FontSize = 100,
                Text = "Rest",
                Background = Brushes.Transparent,
                Foreground = Brushes.White,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
            };
            var grid = new Grid();
            grid.Children.Add(_background);
            grid.Children.Add(_text);
            AddChild(grid);

            KeyDown += ScreenLocker_KeyDown;
            _timer.Tick += Timer_Tick;

            _timer.Start();
        }

        #region Blinking
        private DispatcherTimer _timer = new() { Interval = new TimeSpan(10000) };
        private double _opacityStep = 0.002, border = 0.35;
        private Rectangle _background;
        private TextBlock _text;
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (_background.Opacity + _opacityStep > 1.0 - border  ||
                _background.Opacity + _opacityStep < 0.0 + border)
                _opacityStep *= -1;
            _background.Opacity += _opacityStep;
        }
        public void SetToRed(string message = "LEAVE")
        {
            _text.Text = message;
            (_background.Fill as SolidColorBrush).Color = Color.FromRgb(0xdf, 0x4f, 0x4f);
        }
        public void SetToGreen(string message = "Good")
        {
            _text.Text = message;
            (_background.Fill as SolidColorBrush).Color = Color.FromRgb(0x88, 0xdf, 0x4c);
        }
        public void SetToBlue(string message = "Rest")
        {
            _text.Text = message;
            (_background.Fill as SolidColorBrush).Color = Color.FromRgb(0x41, 0xb7, 0xd7);
        }
        #endregion

        public void Froze()
        {
            _timer.Stop();
        }
        public void Unfroze()
        {
            _timer.Start();
        }

        #region Password
#if !QUICK_PASSWORD
        private readonly string _magicWord = "THANKS";
        private int _index;
#endif
        private void ScreenLocker_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
#if QUICK_PASSWORD
            Close();
#else
            var letter = e.Key.ToString()[0];
            if (letter == _magicWord[_index])
                ++_index;
            else
                _index = 0;
            if (_index == _magicWord.Length)
            {
                _index = 0;
                Close();
            }
#endif
        }
#endregion Password
    }
}
