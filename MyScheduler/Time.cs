﻿using System;

namespace MyScheduler
{
    internal struct Time
    {
        #region Properties
        private Boxed<int> hours, minutes, seconds;
        public int Hours { get => hours.value; set => hours.value = value; }
        public int Minutes { get => minutes.value; set => minutes.value = value; }
        public int Seconds { get => seconds.value; set => seconds.value = value; }
        public int TotalSeconds { get => 3600 * Hours
                + 60 * Minutes + Seconds; }
        static public Time Zero
            = new Time(new Boxed<int>(), new Boxed<int>(), new Boxed<int>());
        static public readonly Time OneSecond
            = new Time(new Boxed<int>(), new Boxed<int>(), new Boxed<int>() { value = 1 });
        public Boxed<int> GetHiddenHours() => hours;
        public Boxed<int> GetHiddenMinutes() => minutes;
        public Boxed<int> GetHiddenSeconds() => seconds;
        #endregion Properties

        #region Constructors
        public Time(Boxed<int> hours, Boxed<int> minutes, Boxed<int> seconds)
        {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }
        public Time(TimeSpan timeSpan)
            :this(new Boxed<int>(), new Boxed<int>(), new Boxed<int>())
        {
            Hours = timeSpan.Hours;
            Minutes = timeSpan.Minutes;
            Seconds = timeSpan.Seconds;
        }
        public Time(int totalSeconds)
            :this(new Boxed<int>(), new Boxed<int>(), new Boxed<int>())
        {
            Hours = totalSeconds / 3600;
            totalSeconds %= 3600;
            Minutes = totalSeconds / 60;
            totalSeconds %= 60;
            Seconds = totalSeconds;
        }
        public Time(Time other)
            :this(new Boxed<int>(), new Boxed<int>(), new Boxed<int>())
        {
            Hours = other.Hours;
            Minutes = other.Minutes;
            Seconds = other.Seconds;
        }
        #endregion Constructors

        #region Operators
        public void Subtruct(Time other)
        {
            var totalSeconds = this.TotalSeconds - other.TotalSeconds;
            this.Hours = totalSeconds / 3600;
            totalSeconds %= 3600;
            this.Minutes = totalSeconds / 60;
            totalSeconds %= 60;
            this.Seconds = totalSeconds;
        }
        public void Add(Time other)
        {
            var totalSeconds = this.TotalSeconds + other.TotalSeconds;
            this.Hours = totalSeconds / 3600;
            totalSeconds %= 3600;
            this.Minutes = totalSeconds / 60;
            totalSeconds %= 60;
            this.Seconds = totalSeconds;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return base.ToString();
        }
        public static bool operator ==(Time a, Time b)
        {
            return a.Hours == b.Hours && a.Minutes == b.Minutes &&
                a.Seconds == b.Seconds;
        }
        public static bool operator !=(Time a, Time b)
        {
            return a.Hours != b.Hours || a.Minutes != b.Minutes ||
                a.Seconds != b.Seconds;
        }
        #endregion
    }
}
