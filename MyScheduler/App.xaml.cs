﻿using System.Windows;

namespace MyScheduler
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            _screenLocker.Closing += ScreenLocker_Closing;
            _scheduler.Closing += Scheduler_Closing;
            _scheduler.OnAlarm += OnAlarm;
        }

        private void AppStartup(object sender, StartupEventArgs e)
        {
            ShowOut(_scheduler);
        }

        #region Reactions
        private void ScreenLocker_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            HideAway(_screenLocker);
            _screenLocker.Froze();
            ShowOut(_scheduler);
        }
        private void Scheduler_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_screenLocker == null)
                return;
            _screenLocker.Closing -= ScreenLocker_Closing;
            _screenLocker.Close();
        }
        private void OnAlarm(bool code_red)
        {
            if (code_red)
            {
                _screenLocker.SetToRed();
            }
            else
            {
                _screenLocker.SetToBlue();
            }
            HideAway(_scheduler);
            _screenLocker.Unfroze();
            ShowOut(_screenLocker);
        }
        #endregion

        #region Static methods
        private static void HideAway(Window window)
        {
            //window.Topmost = false;
            window.Hide();
        }
        private static void ShowOut(Window window)
        {
            //window.Topmost = true;
            window.Show();
            window.Activate();
            window.Focus();
        }
        #endregion

        #region Fields
        private readonly ScreenLocker _screenLocker = new();
        private readonly Scheduler _scheduler = new();
        #endregion
    }
}
