﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace MyScheduler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Scheduler : Window
    {

        public Scheduler()
        {
            InitializeComponent();

            _timer.Tick += Timer_Tick;

            _currentTime = new Time(0);
            SetCurrentTime(_defaultTime);

            RunButton.Click += RunButton_Click;
            StopButton.Click += StopButton_Click;
            ConfirmButton.Click += ConfirmButton_Click;
            ResetButton.Click += ResetButton_Click;
            ReverseButton.MouseDown += ReverseButton_MouseDown;

            KeyDown += Scheduler_KeyDown;
            OnReversedMode += AfterUserReversedMode;

            _timer.Start();
        }

        private void SetCurrentTime(Time time)
        {
            _currentTime.Hours = time.Hours;
            _currentTime.Minutes = time.Minutes;
            _currentTime.Seconds = time.Seconds;
            hoursScroller.SetHiddenNumber(_currentTime.GetHiddenHours());
            minutesScroller.SetHiddenNumber(_currentTime.GetHiddenMinutes());
            secondsScroller.SetHiddenNumber(_currentTime.GetHiddenSeconds());
            UpdateNumberScrollers();
        }
        private void UpdateNumberScrollers()
        {
            hoursScroller.Update();
            minutesScroller.Update();
            secondsScroller.Update();
        }

        #region Button Actions
        private void AfterUserReversedMode()
        {
            switch (_mode)
            {
                case SchedulerMode.Timer:
                    SetCurrentTime(_defaultTime);
                    ReverseButton.Fill = Brushes.PaleGreen;
                    ConfirmButton.Visibility = Visibility.Visible;
                    break;
                case SchedulerMode.Stopwatch:
                    SetCurrentTime(Time.Zero);
                    ReverseButton.Fill = Brushes.LightBlue;
                    ConfirmButton.Visibility = Visibility.Hidden;
                    break;
            }
        }
        private void ReverseButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mode = _mode switch {
                SchedulerMode.Timer => SchedulerMode.Stopwatch,
                _ => SchedulerMode.Timer,
            };
            OnReversedMode();
        }
        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            switch (_mode)
            {
                case SchedulerMode.Timer:
                    SetCurrentTime(_defaultTime);
                    break;
                case SchedulerMode.Stopwatch:
                    SetCurrentTime(Time.Zero);
                    break;
            }
        }
        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
            => _defaultTime = new Time(_currentTime);
        private void StopButton_Click(object sender, RoutedEventArgs e)
            => _timer.Stop();
        private void RunButton_Click(object sender, RoutedEventArgs e)
            => _timer.Start();
        #endregion

        #region Timers & Ticks
        private void Timer_Tick(object sender, EventArgs e)
        {
            switch (_mode)
            {
                case SchedulerMode.Timer:
                    {
                        _currentTime.Subtruct(Time.OneSecond);
                        if (_currentTime == Time.Zero)
                        {
                            _timer.Stop();
                            OnAlarm();
                            _mode = SchedulerMode.Stopwatch;
                            OnReversedMode();
                            _timer.Start();
                        }
                    }
                    break;
                case SchedulerMode.Stopwatch:
                    {
                        _currentTime.Add(Time.OneSecond);
                        if (_currentTime.GetHiddenSeconds().value % _defaultTime.GetHiddenSeconds().value == 0)
                        {
                            OnAlarm(code_red: true);
                        }
                    }
                    break;
                default:
                    break;
            }
            UpdateNumberScrollers();
        }
        #endregion

        #region Other behaviour
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            this.DragMove();
        }
        private void Scheduler_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    Close();
                    break;
            }
        }
        private void LockScreen_Click(object sender, RoutedEventArgs e)
            => OnAlarm();
        #endregion

        #region Events
        public delegate void ReversedMode();
        public event ReversedMode OnReversedMode;
        public delegate void Alarm(bool code_red = false);
        public event Alarm OnAlarm;
        #endregion

        #region Fields
#if RELEASE
        private Time _defaultTime = new Time(new Boxed<int>(),
            new Boxed<int>(){value = 40}, new Boxed<int>());
#else
        private Time _defaultTime = new Time(new Boxed<int>(),
            new Boxed<int>(), new Boxed<int>() { value = 5 });
#endif

        private Time _currentTime;
        private static readonly TimeSpan _oneSecond = new(hours: 0, minutes: 0, seconds: 1);
        private static readonly DispatcherTimer _timer = new() { Interval = _oneSecond };
        private SchedulerMode _mode = SchedulerMode.Timer;
        #endregion
    }
}
