﻿
using System.Windows.Controls;
using System.Windows.Media;

namespace MyScheduler
{
    internal class NewNumberScroller : TextBox
    {
        private string MyNumberAsString()
        {
            if (Value == 0)
                return "00";
            if (Value < 10)
                return "0" + Value.ToString();
            return Value.ToString();
        }

        private void FixMyValue()
        {
            if (Value < MinValue)
                Value += MaxValue + 1;
            if (Value > MaxValue)
                Value -= MaxValue + 1;
        }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int Step { get; set; }

        private Boxed<int> number;
        public void SetHiddenNumber(Boxed<int> number)
        {
            this.number = number;
            Update();
        }
        public int Value
        {
            get => number.value;
            set
            {
                number.value = value;
                FixMyValue();
            }
        }
        public override void BeginInit()
        {
            base.BeginInit();
            FontFamily = new System.Windows.Media.FontFamily("Digital-7 Mono");
            FontSize = 90;
            BorderThickness = new System.Windows.Thickness(3);
            BorderBrush = new SolidColorBrush(Colors.Black);
            Foreground = new SolidColorBrush(Colors.Black);
            HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
            IsReadOnly = true;
            MouseWheel += OnWheel;
            // Defaults
            MinValue = 0;
            MaxValue = 100;
            Step = 1;
            SetHiddenNumber(new Boxed<int>() { value = 0 });
            Text = MyNumberAsString();
        }
        public void Update()
        {
            Text = MyNumberAsString();
        }
        private void OnWheel(object sender,
            System.Windows.Input.MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
                Value += Step;
            if (e.Delta < 0)
                Value -= Step;
            Update();
        }
    }
}
